# install python3.10 and build jupyterlab with chosen extensions
# https://computingforgeeks.com/how-to-install-python-on-ubuntu-linux-system/
sudo apt update && sudo apt upgrade -y
sudo apt install software-properties-common -y
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt install python3.10
python3.10 --version
poetry env use python3.10
poetry install

set ServerApp.allow_remote_access
